<?php

class Trollweb_Clearcart_Model_Observer {
    // Clear cart on logout
    public function customerLogout(Varien_Event_Observer $observer) {
        Mage::log("Clearing cart on logout");
        foreach (Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection() as $item) {
            Mage::getSingleton('checkout/cart')->removeItem($item->getId())->save();
        }
    }

    // Clear cart on login
    public function customerLogin(Varien_Event_Observer $observer) {
        Mage::log("Clearing cart on login");
        foreach (Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection() as $item) {
            Mage::getSingleton('checkout/cart')->removeItem($item->getId())->save();
        }
    }
}
